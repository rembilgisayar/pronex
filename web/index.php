<?php



error_reporting(E_ALL ^ E_NOTICE);
ini_set('error_reporting', E_ALL ^ E_NOTICE);
session_start();


// $user1 = $_SESSION['user'];
 if ($_SESSION['username'] == "") {
header("location:Login.php");
}
else {
	
	include ("inc/db_settings.php");
	include ("inc/guiconfig.inc");
require_once ("inc/header.php");


$sayfa = @$_GET["s"];
if (empty($sayfa) || !is_numeric ($sayfa)) {
$sayfa=1;
}
	?>
<!DOCTYPE html>
<html lang="tr">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0" />
<meta name="description" content="Rem Bilgisayar Güvenlik Duvarı Yönetim Sistemi" />
<meta name="author" content="Rem Bilgisayar" />
<title>RemBilgisayar HotSpot-System</title>
<link rel="stylesheet" href="assets/js/jquery-ui/css/no-theme/jquery-ui-1.10.3.custom.min.css">
<link rel="stylesheet" href="assets/css/font-icons/entypo/css/entypo.css">
<link rel="stylesheet" href="http://fonts.googleapis.com/css?family=Noto+Sans:400,700,400italic">
<link rel="stylesheet" href="assets/css/bootstrap.css">
<link rel="stylesheet" href="assets/css/neon-core.css">
<link rel="stylesheet" href="assets/css/neon-theme.css">
<link rel="stylesheet" href="assets/css/neon-forms.css">
<link rel="stylesheet" href="assets/css/custom.css">
<script src="assets/js/jquery-1.11.0.min.js"></script>
<script>$.noConflict();</script>

</head>
<body class="page-body  page-fade" data-url="http://neon.dev">
<div class="page-container"><!-- add class "sidebar-collapsed" to close sidebar by default, "chat-visible" to make chat appear always -->
  
  <div class="sidebar-menu">
    <div class="sidebar-menu-inner"> 
      <!-- Header Başlangıç -->
      <header class="logo-env"> 
        
        <!-- logo -->
        <div class="logo"> <a href="index.php"> <img src="assets/images/logo.png" width="120"   alt="" /> </a> </div>
        
        <!-- logo collapse icon -->
        <div class="sidebar-collapse"> <a href="#" class="sidebar-collapse-icon"><!-- add class "with-animation" if you want sidebar to have animation during expanding/collapsing transition --> 
          <i class="entypo-menu"></i> </a> </div>
        
        <!-- open/close menu icon (do not remove if you want to enable menu on mobile devices) -->
        <div class="sidebar-mobile-menu visible-xs"> <a href="#" class="with-animation"><!-- add class "with-animation" to support animation --> 
          <i class="entypo-menu"></i> </a> </div>
      </header>
      <!-- Header Bitiş --> 
      
      <!-- Menu Başlangıç -->
      <ul id="main-menu" class="main-menu">
        <!-- add class "multiple-expanded" to allow multiple submenus to open --> 
        <!-- class "auto-inherit-active-class" will automatically add "active" class for parent elements who are marked already with class "active" -->
        <li class="active "> <a href="#"> <i class="entypo-users"></i> <span class="title">Kullanıcı Yönetimi</span> </a>
          <ul>
            <li class="active"> <a href="index.php?Islem=yenikullanici"> <span class="title">Kullanıcı Tanımla</span> </a> </li>
            <li> <a href="index.php?Islem=kullanicilistesi"> <span class="title">Kullanıcı Listesi</span> </a> </li>
            <li> <a href="index.php?Islem=arama"> <span class="title">Kullanıcı Ara</span> </a> </li>
          </ul>
        </li>
        <li> <a href="#"> <i class="entypo-layout"></i> <span class="title">Log & Kayıt Yönetimi</span> </a>
          <ul>
            <li> <a href="index.php?Islem=online"> <span class="title">Online Kullanıcılar</span> </a> </li>
            <li> <a href="index.php?Islem=hareketler"> <span class="title">Giriş - Çıkış Kayıtları</span> </a> </li>
            <li> <a href="index.php?Islem=data"> <span class="title">Data Kullanım Kayıtları</span> </a> </li>
            <li> <a href="index.php?Islem=5651"> <span class="title">5651 Loglar</span> </a> </li>
             <li> <a href="https://192.168.1.1:7445"> <span class="title">Kullanım Detayları</span> </a> </li>
          </ul>
        </li>
        <li> <a href="#"> <i class="entypo-newspaper"></i> <span class="title">Sistem Ayarları</span> </a>
          <ul>
            <li> <a href="index.php?Islem=ayarlar"> <span class="title"><i class="entypo-chart-bar"></i> Hot Spot Ayarları</span> </a> </li>
            <li> <a href="#"> <span class="title"><i class="entypo-shuffle"></i> Firewall Ayarları</span> </a> 
            <ul>
             <li> <a href="index.php?Islem=firewall/nat"> <span class="title"><i class="entypo-shuffle"></i> NAT Ayarları</span> </a> 
               <li> <a href="index.php?Islem=firewall/sabit-ip"> <span class="title"><i class="entypo-flow-tree"> </i></i>Sabit ip Ayarları</span> </a> 
                <li> <a href="#"> <span class="title"><i class="entypo-finger-print"> </i> Web Filitreleme</span> </a> 
                <ul>
                <li> <a href="index.php?Islem=firewall/beyazliste"> <span class="title">Beyaz Liste</span> </a> 
                <li> <a href="index.php?Islem=firewall/karaliste"> <span class="title">Kara Liste</span> </a> 
                <li> <a href="index.php?Islem=firewall/kategori"> <span class="title">Kategori Filitreleme</span> </a>
                    <ul>
                        <li><a href="index.php?Islem=firewall/zaman"> <span class="title">Zaman Bazlı Kategori Filitreleme</span> </a> </li>
                    </ul>
                </ul>
            </ul> 
             
            </li>
            <li> <a href="index.php?Islem=pwd"> <span class="title">Sistem Şifresi Değiştirme</span> </a> </li>
          </ul>
        </li>

       <li> <a href="index.php?Islem=cikis"> <i class="entypo-chart-bar"></i> <span class="title">Çıkış Yap</span> </a> </li>
      </ul>
      <!-- Menu Bitiş --> 
      
    </div> 
  </div>
  <div class="main-content">
    <div class="row"> 
      
      <!-- Profile Info and Notifications -->
      <div class="col-md-6 col-sm-8 clearfix">
        <ul class="user-info pull-left pull-none-xsm">
          
          <!-- Profile Info -->
          <li class="profile-info dropdown"><!-- add class "pull-right" if you want to place this from right --> 
            
            <a href="#" class="dropdown-toggle" data-toggle="dropdown"> Sistem Yöneticisi </a>
            <ul class="dropdown-menu">
              
              <!-- Reverse Caret -->
              <li class="caret"></li>
              
              <!-- Profile sub-links -->
              <li> <a href="index.php?Islem=pwd"> <i class="entypo-user"></i> Şifre Değiştir </a> </li>
              <li> <a href="index.php?Islem=cikis"> <i class="entypo-user"></i> Çıkış Yap </a> </li>
            </ul>
          </li>
        </ul>
      </div>
      
      <!-- Raw Links -->
      <div class="col-md-6 col-sm-4 clearfix hidden-xs">
        <ul class="list-inline links-list pull-right">
          <li class="sep"></li>
          <li> <a href="index.php?Islem=cikis"> Çıkış Yap <i class="entypo-logout right"></i> </a> </li>
        </ul>
      </div>
    </div>
    <hr />
    <?php if($_GET[Islem]) {
			
			if ( file_exists("theme/$_GET[Islem].php") ) 
			{
include("theme/$_GET[Islem].php");
			} else { include("theme/404.php"); }
			
		}
			else { include("theme/dashboard.php"); }
			 
			
			
			
		?>
    
    <!-- Footer -->
    <footer class="main"> &copy; 2016 <strong>RemBilgisayar Hotspot System</strong> Powered By<a href="http://rembilgisayar.com.tr" target="_blank"> Rem bilgisayar</a> </footer>
  </div>
</div>

<!-- Imported styles on this page -->
<link rel="stylesheet" href="assets/js/jvectormap/jquery-jvectormap-1.2.2.css">
<link rel="stylesheet" href="assets/js/rickshaw/rickshaw.min.css">

<!-- Bottom scripts (common) --> 
<script src="assets/js/gsap/main-gsap.js"></script> 
<script src="assets/js/jquery-ui/js/jquery-ui-1.10.3.minimal.min.js"></script> 
<script src="assets/js/bootstrap.js"></script> 
<script src="assets/js/joinable.js"></script> 
<script src="assets/js/resizeable.js"></script> 
<script src="assets/js/neon-api.js"></script> 
<script src="assets/js/jvectormap/jquery-jvectormap-1.2.2.min.js"></script> 

<!-- Imported scripts on this page --> 
<script src="assets/js/jvectormap/jquery-jvectormap-europe-merc-en.js"></script> 
<script src="assets/js/jquery.sparkline.min.js"></script> 
<script src="assets/js/rickshaw/vendor/d3.v3.js"></script> 
<script src="assets/js/rickshaw/rickshaw.min.js"></script> 
<script src="assets/js/raphael-min.js"></script> 
<script src="assets/js/morris.min.js"></script> 
<script src="assets/js/toastr.js"></script> 
<!-- Imported scripts on this page --> 
<script src="assets/js/bootstrap-switch.min.js"></script> 
<script src="assets/js/jquery.validate.min.js"></script> 
<script src="assets/js/neon-chat.js"></script> 
<script src="assets/js/bootstrap-datepicker.js"></script> 

<!-- JavaScripts initializations and stuff --> 
<script src="assets/js/neon-custom.js"></script> 

<!-- Demo Settings --> 
<script src="assets/js/neon-demo.js"></script>
</body>
</html>
<?php 

	 } ?>
