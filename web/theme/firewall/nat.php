<?php

require_once("functions.inc");
require_once("filter.inc");
require_once("shaper.inc");
require_once("itemid.inc");

if (!is_array($config['nat']['rule'])) {
	$config['nat']['rule'] = array();
}

$a_nat = &$config['nat']['rule'];

/* update rule order, POST[rule] is an array of ordered IDs */
if (array_key_exists('order-store', $_POST)) {
	if (is_array($_POST['rule']) && !empty($_POST['rule'])) {
		$a_nat_new = array();

		// if a rule is not in POST[rule], it has been deleted by the user
		foreach ($_POST['rule'] as $id) {
			$a_nat_new[] = $a_nat[$id];
		}

		$a_nat = $a_nat_new;


		$config['nat']['separator'] = "";

		if ($_POST['separator']) {
			$idx = 0;
			foreach ($_POST['separator'] as $separator) {
				$config['nat']['separator']['sep' . $idx++] = $separator;
			}
		}

		if (write_config()) {
			mark_subsystem_dirty('filter');
		}

		header("Location: firewall_nat.php");
		exit;
	}
}

/* if a custom message has been passed along, lets process it */
if ($_GET['savemsg']) {
	$savemsg = $_GET['savemsg'];
}

if ($_POST) {
	$pconfig = $_POST;

	if ($_POST['apply']) {

		$retval = 0;

		$retval |= filter_configure();
		$savemsg = get_std_save_message($retval);

		pfSense_handle_custom_code("/usr/local/pkg/firewall_nat/apply");

		if ($retval == 0) {
			clear_subsystem_dirty('natconf');
			clear_subsystem_dirty('filter');
		}

	}
}

if ($_GET['act'] == "del") {
	if ($a_nat[$_GET['id']]) {

		if (isset($a_nat[$_GET['id']]['associated-rule-id'])) {
			delete_id($a_nat[$_GET['id']]['associated-rule-id'], $config['filter']['rule']);
			$want_dirty_filter = true;
		}
		unset($a_nat[$_GET['id']]);

		// Update the separators
		$a_separators = &$config['nat']['separator'];
		$ridx = $_GET['id'];
		$mvnrows = -1;
		move_separators($a_separators, $ridx, $mvnrows);

		if (write_config()) {
			mark_subsystem_dirty('natconf');
			if ($want_dirty_filter) {
				mark_subsystem_dirty('filter');
			}
		}

		header("Location: firewall_nat.php");
		exit;
	}
}

if (isset($_POST['del_x'])) {
	/* delete selected rules */
	if (is_array($_POST['rule']) && count($_POST['rule'])) {
		$a_separators = &$config['nat']['separator'];
		$num_deleted = 0;

		foreach ($_POST['rule'] as $rulei) {
			$target = $rule['target'];

			// Check for filter rule associations
			if (isset($a_nat[$rulei]['associated-rule-id'])) {
				delete_id($a_nat[$rulei]['associated-rule-id'], $config['filter']['rule']);
				mark_subsystem_dirty('filter');
			}

			unset($a_nat[$rulei]);

			// Update the separators
			// As rules are deleted, $ridx has to be decremented or separator position will break
			$ridx = $rulei - $num_deleted;
			$mvnrows = -1;
			move_separators($a_separators, $ridx, $mvnrows);
			$num_deleted++;
		}

		if (write_config()) {
			mark_subsystem_dirty('natconf');
		}

		header("Location: firewall_nat.php");
		exit;
	}
} else if ($_GET['act'] == "toggle") {
	if ($a_nat[$_GET['id']]) {
		if (isset($a_nat[$_GET['id']]['disabled'])) {
			unset($a_nat[$_GET['id']]['disabled']);
		} else {
			$a_nat[$_GET['id']]['disabled'] = true;
		}
		if (write_config(gettext("Firewall: NAT: Port forward, enable/disable NAT rule"))) {
			mark_subsystem_dirty('natconf');
		}
		header("Location: firewall_nat.php");
		exit;
	}
}

	
if ($savemsg) {
	print_info_box($savemsg, 'success');
}

if (is_subsystem_dirty('natconf')) {
	print_apply_box(gettext('The NAT configuration has been changed.') . '<br />' .
					gettext('The changes must be applied for them to take effect.'));
}



$columns_in_table = 13;
?>

<form action="firewall_nat.php" method="post" name="iform">
	<div class="panel panel-default">
		<div class="panel-heading"><h2 class="panel-title"><?=gettext('Kurallar')?></h2></div>
		<div class="panel-body table-responsive">
			<table id="ruletable" class="table table-striped table-hover table-condensed">
				<thead>
					<tr>
						<th><!-- Checkbox --></th>
						<th><!-- Icon --></th>
						<th><!-- Rule type --></th>
						<th><?=gettext("Arabirim")?></th>
						<th><?=gettext("Protokol")?></th>
						<th><?=gettext("Kaynak Adresi")?></th>
						<th><?=gettext("Kaynak Portu")?></th>
						<th><?=gettext("Çıkış Adresi")?></th>
						<th><?=gettext("Çıkış Portı")?></th>
						<th><?=gettext("NAT IP")?></th>
						<th><?=gettext("NAT Portu")?></th>
						<th><?=gettext("Açıklama")?></th>
						<th><?=gettext("İşlem")?></th>
					</tr>
				</thead>
				<tbody class='user-entries'>
<?php

$nnats = $i = 0;
$separators = $config['nat']['separator'];

// Get a list of separator rows and use it to call the display separator function only for rows which there are separator(s).
// More efficient than looping through the list of separators on every row.
$seprows = separator_rows($separators);

foreach ($a_nat as $natent):

	// Display separator(s) for section beginning at rule n
	if ($seprows[$nnats]) {
		display_separator($separators, $nnats, $columns_in_table);
	}

	$localport = $natent['local-port'];

	list($dstbeginport, $dstendport) = explode("-", $natent['destination']['port']);

	if ($dstendport) {
		$localendport = $natent['local-port'] + $dstendport - $dstbeginport;
		$localport	 .= '-' . $localendport;
	}

	$alias = rule_columns_with_alias(
		$natent['source']['address'],
		pprint_port($natent['source']['port']),
		$natent['destination']['address'],
		pprint_port($natent['destination']['port']),
		$natent['target'],
		$localport
	);

	/* if user does not have access to edit an interface skip on to the next record */
	if (!have_natpfruleint_access($natent['interface'])) {
		continue;
	}

	if (isset($natent['disabled'])) {
		$iconfn = "pass_d";
		$trclass = 'class="disabled"';
	} else {
		$iconfn = "pass";
		$trclass = '';
	}
?>

					<tr id="fr<?=$nnats;?>" <?=$trclass?> onClick="fr_toggle(<?=$nnats;?>)" ondblclick="document.location='firewall_nat_edit.php?id=<?=$i;?>';">
						<td >
							<input type="checkbox" id="frc<?=$nnats;?>" onClick="fr_toggle(<?=$nnats;?>)" name="rule[]" value="<?=$i;?>"/>
						</td>
						<td>
							<a href="?act=toggle&amp;id=<?=$i?>">
								<i class="fa <?= ($iconfn == "pass") ? "fa-check":"fa-times"?>" title="<?=gettext("click to toggle enabled/disabled status")?>"></i>
<?php 	if (isset($natent['nordr'])) { ?>
								&nbsp;<i class="fa fa-hand-stop-o text-danger" title="<?=gettext("Negated: This rule excludes NAT from a later rule")?>"></i>
<?php 	} ?>
							</a>
						</td>
						<td>
<?php
	if ($natent['associated-rule-id'] == "pass"):
?>
							<i class="fa fa-play" title="<?=gettext("All traffic matching this NAT entry is passed")?>"></i>
<?php
	elseif (!empty($natent['associated-rule-id'])):
?>
							<i class="fa fa-random" title="<?=sprintf(gettext("Firewall rule ID %s is managed by this rule"), htmlspecialchars($natent['associated-rule-id']))?>"></i>
<?php
	endif;
?>
						</td>
						<td>
							<?=$textss?>
<?php
	if (!$natent['interface']) {
		echo htmlspecialchars(convert_friendly_interface_to_friendly_descr("wan"));
	} else {
		echo htmlspecialchars(convert_friendly_interface_to_friendly_descr($natent['interface']));
	}
?>
							<?=$textse?>
						</td>

						<td>
							<?=$textss?><?=strtoupper($natent['protocol'])?><?=$textse?>
						</td>

						<td>


<?php
	if (isset($alias['src'])):
?>
							<a href="/firewall_aliases_edit.php?id=<?=$alias['src']?>" data-toggle="popover" data-trigger="hover focus" title="<?=gettext('Alias details')?>" data-content="<?=alias_info_popup($alias['src'])?>" data-html="true">
<?php
	endif;
?>
							<?=str_replace('_', ' ', htmlspecialchars(pprint_address($natent['source'])))?>
<?php
	if (isset($alias['src'])):
?>
							</a>
<?php
	endif;
?>
						</td>
						<td>
<?php
	if (isset($alias['srcport'])):
?>
							<a href="/firewall_aliases_edit.php?id=<?=$alias['srcport']?>" data-toggle="popover" data-trigger="hover focus" title="<?=gettext('Alias details')?>" data-content="<?=alias_info_popup($alias['srcport'])?>" data-html="true">
<?php
	endif;
?>
							<?=str_replace('_', ' ', htmlspecialchars(pprint_port($natent['source']['port'])))?>
<?php
	if (isset($alias['srcport'])):
?>
							</a>
<?php
	endif;
?>
						</td>

						<td>
<?php
	if (isset($alias['dst'])):
?>
							<a href="/firewall_aliases_edit.php?id=<?=$alias['dst']?>" data-toggle="popover" data-trigger="hover focus" title="<?=gettext('Alias details')?>" data-content="<?=alias_info_popup($alias['dst'])?>" data-html="true">
<?php
	endif;
?>
							<?=str_replace('_', ' ', htmlspecialchars(pprint_address($natent['destination'])))?>
<?php
	if (isset($alias['dst'])):
?>
							</a>
<?php
	endif;
?>
						</td>
						<td>
<?php
	if (isset($alias['dstport'])):
?>
							<a href="/firewall_aliases_edit.php?id=<?=$alias['dstport']?>" data-toggle="popover" data-trigger="hover focus" title="<?=gettext('Alias details')?>" data-content="<?=alias_info_popup($alias['dstport'])?>" data-html="true">
<?php
	endif;
?>
							<?=str_replace('_', ' ', htmlspecialchars(pprint_port($natent['destination']['port'])))?>
<?php
	if (isset($alias['dstport'])):
?>
							</a>
<?php
	endif;
?>
						</td>
						<td>
<?php
	if (isset($alias['target'])):
?>
							<a href="/firewall_aliases_edit.php?id=<?=$alias['target']?>" data-toggle="popover" data-trigger="hover focus" title="<?=gettext('Alias details')?>" data-content="<?=alias_info_popup($alias['target'])?>" data-html="true">
<?php
	endif;
?>

							<?=str_replace('_', ' ', htmlspecialchars($natent['target']))?>
<?php
	if (isset($alias['target'])):
?>
							</a>
<?php
	endif;
?>
						</td>
						<td>
<?php
	if (isset($alias['targetport'])):
?>
							<a href="/firewall_aliases_edit.php?id=<?=$alias['targetport']?>" data-toggle="popover" data-trigger="hover focus" title="<?=gettext('Alias details')?>" data-content="<?=alias_info_popup($alias['targetport'])?>" data-html="true">
<?php
	endif;
?>
							<?=str_replace('_', ' ', htmlspecialchars(pprint_port($localport)))?>
<?php
	if (isset($alias['targetport'])):
?>
							</a>
<?php
	endif;
?>
						</td>

						<td>
							<?=htmlspecialchars($natent['descr'])?>
						</td>
						<td>
							<a class="fa fa-pencil" title="<?=gettext("Edit rule"); ?>" href="firewall_nat_edit.php?id=<?=$i?>"></a>
							<a class="fa fa-clone"	  title="<?=gettext("Add a new NAT based on this one")?>" href="firewall_nat_edit.php?dup=<?=$i?>"></a>
							<a class="fa fa-trash"	title="<?=gettext("Delete rule")?>" href="firewall_nat.php?act=del&amp;id=<?=$i?>"></a>
						</td>
					</tr>
<?php
	$i++;
	$nnats++;

endforeach;

// There can be separator(s) after the last rule listed.
if ($seprows[$nnats]) {
	display_separator($separators, $nnats, $columns_in_table);
}
?>
				</tbody>
			</table>
		</div>
	</div>

	<nav class="action-buttons">
		<a href="index.php?Islem=firewall/nat-ekle" class="btn btn-sm btn-success" title="<?=gettext('Add rule to the top of the list')?>">
			<i class="fa fa-level-up icon-embed-btn"></i>
			<?=gettext('Yukarı Ekle')?>
		</a>
		<a href="index.php?Islem=firewall/nat-ekle" class="btn btn-sm btn-success" title="<?=gettext('Add rule to the end of the list')?>">
			<i class="fa fa-level-down icon-embed-btn"></i>
			<?=gettext('Aşağıya ekle')?>
		</a>
		<button name="del_x" type="submit" class="btn btn-danger btn-sm" title="<?=gettext('Delete selected rules')?>">
			<i class="fa fa-trash icon-embed-btn"></i>
			<?=gettext("Sil"); ?>
		</button>
		<button type="submit" id="order-store" name="order-store" class="btn btn-primary btn-sm" disabled title="<?=gettext('Save rule order')?>">
			<i class="fa fa-save icon-embed-btn"></i>
			<?=gettext("Kaydet")?>
		</button>
		
	</nav>
</form>

<script type="text/javascript">
//<![CDATA[
//Need to create some variables here so that jquery/pfSenseHelpers.js can read them
iface = "<?=strtolower($if)?>";
cncltxt = '<?=gettext("Cancel")?>';
svtxt = '<?=gettext("Save")?>';
svbtnplaceholder = '<?=gettext("Enter a description, Save, then drag to final location.")?>';
configsection = "nat";
dirty = false;

events.push(function() {

	// Make rules sortable
	$('table tbody.user-entries').sortable({
		cursor: 'grabbing',
		update: function(event, ui) {
			$('#order-store').removeAttr('disabled');
			dirty = true;
			reindex_rules(ui.item.parent('tbody'));
			dirty = true;
		}
	});

	// Check all of the rule checkboxes so that their values are posted
	$('#order-store').click(function () {
	   $('[id^=frc]').prop('checked', true);

		// Save the separator bar configuration
		save_separators();

		// Suppress the "Do you really want to leave the page" message
		saving = true;

	});

	// Globals
	saving = false;
	dirty = false;

	// provide a warning message if the user tries to change page before saving
	$(window).bind('beforeunload', function(){
		if (!saving && dirty) {
			return ("<?=gettext('One or more Port Forward rules have been moved but have not yet been saved')?>");
		} else {
			return undefined;
		}
	});
});
//]]>
</script>
