
					<ol class="breadcrumb bc-3" >
								<li>
						<a href="index.php"><i class="fa-home"></i>Dashbord</a>
					</li>
							<li>
		
									<a href="index.php">Kullanıcı İşlemleri</a>
							</li>
						<li class="active">
		
									<strong>Yeni Kullanıcı Tanımlama</strong>
							</li>
							</ol>
					
		<h2>HotSpot Kullanıcı Tanımlama</h2>
		
        
        <ul class="nav nav-tabs bordered"><!-- available classes "bordered", "right-aligned" -->
					<li class="active">
						<a href="#suresiz" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-home"></i></span>
							<span class="hidden-xs">Süresiz Kullanıcı</span>
						</a>
					</li>
					<li>
						<a href="#saatlik" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-user"></i></span>
							<span class="hidden-xs">Saat Bazlı Kullanıcı</span>
						</a>
					</li>
					<li>
						<a href="#gunluk" data-toggle="tab">
							<span class="visible-xs"><i class="entypo-mail"></i></span>
							<span class="hidden-xs">Gün Bazlı Kullanıcı</span>
						</a>
					</li>
					
				</ul>
				
				<div class="tab-content">
					<div class="tab-pane active" id="suresiz">
						
						<div class="" data-height="120">
							
                           <div class="panel-body">
		
				<form role="form" id="form1" method="post" class="validate" action="Save.php?id=NewGhostUser">
		
					<div class="form-group">
						<label class="control-label">Kullanıcı Adı (TC Kimlik No)</label>
		
						<input type="text" class="form-control" name="username" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Kullanıcı Adı Giriniz" id="username" />
					</div>
                    <div class="form-group">
						<label class="control-label">Şifre</label>
		
						<input type="password" class="form-control" name="value" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Şifre Giriniz" id="value" />
					</div>
                    <div class="form-group">
						<label class="control-label">Ad Soyad</label>
		
						<input type="text" class="form-control" name="adsoyad" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Kullanıcı Adı Soyadı" id="adsoyad" />
					</div>
                     <div class="form-group">
						<label class="control-label">TC Kimlik No</label>
		
						<input type="text" class="form-control" name="tcno" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="TM Kimlik No Giriniz" id="tcno" />
					</div>
                    
                  <div class="form-group">
					<label class="control-label">Telefon</label>
		
					  <input type="text" class="form-control" name="telefon" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Telefon No Giriniz" id="telefon" />
					</div>
                    <div class="form-group">
						<label class="control-label">E-Posta Adresi</label>
		
						<input type="text" class="form-control" name="eposta" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="E-Posta Adresi Giriniz" id="eposta" />
					</div>
		<input type="hidden" name="tip" value="1"   class="inp-form" />
         <input type="hidden" name="attribute" value="User-Password"   class="inp-form" />
        <input type="hidden" name="op" value="=="   class="inp-form" />
					<div class="form-group"></div>
<div class="form-group">
		    <button type="submit" class="btn btn-success">Gönder</button>
						<button type="reset" class="btn">Sıfırla</button>
				  </div>
		
				</form>
		
			</div>
				
						</div>
						
					</div>
					<div class="tab-pane" id="saatlik">
						
                           <div class="panel-body">
		
				<form role="form" id="form1" method="post" class="validate" action="Save.php?id=Newhours">
		
					<div class="form-group">
						<label class="control-label">Kullanıcı Adı (TC Kimlik No)</label>
						<input type="text" class="form-control" name="username" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Kullanıcı Adı Giriniz" id="username" />
					</div>
                    <div class="form-group">
						<label class="control-label">Şifre</label>
						<input type="password" class="form-control" name="value" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Şifre Giriniz" id="value" />
                    </div>
                    <div class="form-group">
						<label class="control-label">Ad Soyad</label>
		
						<input type="text" class="form-control" name="adsoyad" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Kullanıcı Adı Soyadı" id="adsoyad" />
					</div>
                     <div class="form-group">
						<label class="control-label">TC Kimlik No</label>
		
						<input type="text" class="form-control" name="tcno" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="TM Kimlik No Giriniz" id="tcno" />
					</div>
                  <div class="form-group">
					<label class="control-label">Telefon</label>
		
					 <input type="text" class="form-control" name="telefon" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Telefon Giriniz" id="telefon" />
					</div>
                    <div class="form-group">
						<label class="control-label">E-Posta Adresi</label>
		
						<input type="text" class="form-control" name="eposta" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="E-Posta Adresi Giriniz" id="eposta" />
					</div>
                    <div class="form-group">
						<label class="control-label">Süre Seçiniz</label>
		
						<select name="zaman" class="form-control" id="zaman">
						  <option value="1800">30 DK</option>
						  <option value="3600">1 Saat</option>
						  <option value="7200">2 Saat</option>
						  <option value="10800">3 Saat</option>
						  <option value="14400">4 Saat</option>
						  <option value="18000">5 Saat</option>
                        </select>
					</div>
		
					<div class="form-group"></div>
<div class="form-group">
		    <button type="submit" class="btn btn-success">Gönder</button>
						<button type="reset" class="btn">Sıfırla</button>
				  </div>
		
				</form>
		
			</div>
							
					</div>
					<div class="tab-pane" id="gunluk">
						
                        <div class="panel-body">
		
				<form role="form" id="form1" method="post" class="validate" action="Save.php?id=NewTGhost">
		
					<div class="form-group">
						<label class="control-label">Kullanıcı Adı (TC Kimlik No)</label>
		
						<input type="text" class="form-control" name="username" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Kullanıcı Adı Giriniz" id="username" />
					</div>
                    <div class="form-group">
						<label class="control-label">Şifre</label>
		
						<input type="password" class="form-control" name="value" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Şifre Giriniz" id="value" />
					</div>
                    <div class="form-group">
						<label class="control-label">Ad Soyad</label>
		
						<input type="text" class="form-control" name="adsoyad" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="Kullanıcı Adı Soyadı" id="adsoyad" />
					</div>
                     <div class="form-group">
						<label class="control-label">TC Kimlik No</label>
		
						<input type="text" class="form-control" name="tcno" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="TM Kimlik No Giriniz" id="tcno" />
					</div>
                    <div class="form-group">
						<label class="control-label">E-Posta Adresi</label>
		
						<input type="text" class="form-control" name="eposta" data-validate="required" data-message-required="Bu alanı boş bırakamazsınız" placeholder="E-Posta Adresi Giriniz" id="eposta" />
					</div>
                    <div class="form-group">
						<label class="control-label">Zamanlama</label>
		
						<input name="zaman" type="text" class="form-control datepicker" id="zaman" >
					</div>
		
					<div class="form-group"></div>
<div class="form-group">
		    <button type="submit" class="btn btn-success">Gönder</button>
						<button type="reset" class="btn">Sıfırla</button>
				  </div>
		
				</form>
		
			</div>	
                           
					</div>
					
					
				</div>
				
                <!-- Imported scripts on this page -->

	
