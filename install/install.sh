#!/usr/bin/env bash

main() {
touch ${PWD}/PRHotspot.log
OUTPUTLOG=${PWD}/PRHotspot.log
printf "\033c"
echo "pronex kurulumuna hosgeldiniz."
echo "pronex tercihen temiz kurulmus, daha onceden hotspot kurulumu yapilmamis pfSense 2.3 surumu ve uzerinde test edilmistir."
echo "Bu isleme baslamadan once sisteminizin yedegini almayi unutmayin !!"
echo "Sisteminize gelebilecek zararlardan hic bir sekilde meshul degiliz."
echo "Kurulum ile ilgili kayitlar ${OUTPUTLOG} dosyasina yazilacaktir."
echo "cat ${OUTPUTLOG} |more"
echo "komutu ile kurulum detaylarini inceleyebilirsiniz."

echo "Iptal etmek icin Ctrl+C yapin."
echo
# Defaults
PR_PORT_DEFAULT="3003"
PR_MYSQL_ROOT_PASS_DEFAULT="pronex"
PR_MYSQL_USER_NAME_DEFAULT="pronex"
PR_MYSQL_USER_PASS_DEFAULT="pronex"
PR_MYSQL_DBNAME_DEFAULT="pronex"

# User Inputs
_userInputs

echo
echo "Kurulum basliyor..."
echo

exec 3>&1 1>>${OUTPUTLOG} 2>&1

# FreeBSD ve pfSense paketleri aktif ediliyor...

# Gerekli paketler kuruluyor...
_installPackages

# PRHotspot Repodan cekiliyor...
_clonepronex

# MySQL 5.6 Server paketi kuruluyor...
_mysqlInstall

_mysqlSettings

# PRotspot nginx 3002 port eklemesi yapiliyor...
_nginxSettings



# FreeRADIUS3 kuruluyor...
_radiusInstall

# Cron kuruluyor...
_cronInstall

# PRotspot Yönetim Paneli paketleri kuruluyor...
_prpanel

# PRotspot Proxy paketleri kuruluyor...

_proxy

# PRotspot 5651 paketleri kuruluyor...

_5651

# PRHotspot Konfigurasyon yukleniyor...
_PRHotspotSettings

# FreeBSD ve pfSense paketleri deaktif ediliyor...
if $( YesOrNo "Unifi Controller kurulsun mu ?"); then 1>&3
    echo -n "PRHotspot : Unifi Controller kuruluyor...(Islem uzun surebilir)" 1>&3
    fetch -o - https://git.io/j7Jy | sh -s
    echo " [Tamam]" 1>&3
fi
if $( YesOrNo "Sistem yeniden baslatilsin mi ?"); then 1>&3
        echo "PRHotspot : Sistem yeniden baslatiliyor..." 1>&3
        /sbin/reboot
else
        cd /usr/local/pronex
fi
}

_userInputs() {
    read -p "PRHotspot Web panel portu (Değiştirilmesi önerilmez) [$PR_PORT_DEFAULT]: " PR_PORT
    PR_PORT="${PR_PORT:-$PR_PORT_DEFAULT}"
    read -p "PRHotspot MySQL root sifresi (Değiştirilmesi önerilmez) [$PR_MYSQL_ROOT_PASS_DEFAULT]: " PR_MYSQL_ROOT_PASS
    PR_MYSQL_ROOT_PASS="${PR_MYSQL_ROOT_PASS:-$PR_MYSQL_ROOT_PASS_DEFAULT}"
    read -p "PRHotspot MySQL radius kullanici adi (Değiştirilmesi önerilmez) [$PR_MYSQL_USER_NAME_DEFAULT]: " PR_MYSQL_USER_NAME
    PR_MYSQL_USER_NAME="${PR_MYSQL_USER_NAME:-$PR_MYSQL_USER_NAME_DEFAULT}"
    read -p "PRHotspot MySQL radius kullanici sifresi (Değiştirilmesi önerilmez) [$PR_MYSQL_USER_PASS_DEFAULT]: " PR_MYSQL_USER_PASS
    PR_MYSQL_USER_PASS="${PR_MYSQL_USER_PASS:-$PR_MYSQL_USER_PASS_DEFAULT}"
    read -p "PRHotspot MySQL radius veritabani adi (Değiştirilmesi önerilmez) [$PR_MYSQL_DBNAME_DEFAULT]: " PR_MYSQL_DBNAME
    PR_MYSQL_DBNAME="${PR_MYSQL_DBNAME:-$PR_MYSQL_DBNAME_DEFAULT}"
}


_installPackages() {
    echo -n "PRHotspot : Gerekli paketler kuruluyor... (Islem uzun surebilir)" 1>&3
    ARCH=$(uname -m | sed 's/x86_//;s/i[3-6]86/32/')
    if [ ${ARCH} == "amd64" ]
    then
     echo -n "Sisteminiz 64 bit " 1>&3
    env ASSUME_ALWAYS_YES=YES /usr/sbin/pkg install git wget nano mc htop mysql56-server compat8x-amd64 php56-mysql php56-mysqli php56-pdo_mysql php56-soap
    else
         echo -n "Sisteminiz 32 bit" 1>&3

    env ASSUME_ALWAYS_YES=YES /usr/sbin/pkg install git wget nano mc htop mysql56-server compat8x-i386 php56-mysql php56-mysqli php56-pdo_mysql php56-soap
    fi
    hash -r
    echo " [Tamam]" 1>&3
}

_clonepronex() {
    echo -n "Pronex : Pronex Repo'dan cekiliyor..." 1>&3
    cd /usr/local
    git clone https://semihtore@bitbucket.org/rembilgisayar/pronex.git
    cd /usr/local/pronex
    mv .env.example .env
    cd /usr/local/pronex/install
    echo " [Tamam]" 1>&3
}

_mysqlInstall() {
    echo -n "pronex : MySQL 5.6 Server paketi kuruluyor..." 1>&3
    if [ ! -f /etc/rc.conf.local ] || [ $(grep -c mysql_enable /etc/rc.conf.local) -eq 0 ]; then
        echo 'mysql_enable="YES"' >> /etc/rc.conf.local
    fi
    mv /usr/local/etc/rc.d/mysql-server /usr/local/etc/rc.d/mysql-server.sh
    sed -i .bak -e "s/mysql_enable=\"NO\"/mysql_enable=\"YES\"/g" /usr/local/etc/rc.d/mysql-server.sh
    service mysql-server.sh start
    echo " [Tamam]" 1>&3
}

_mysqlSettings() {
    # MySQL root kullanicisi tanimlaniyor.
    echo -n "pronex : MySQL root kullanicisi tanimlaniyor." 1>&3
    mysqladmin -u root password "${PR_MYSQL_ROOT_PASS}"
    echo " [Tamam]" 1>&3

    # MySQL veritabani yukleniyor
    echo -n "PRotspinstallot : MySQL veritabani yukleniyor" 1>&3
    cat <<EOF > /usr/local/pronex/install/client.cnf
[client]
user = root
password = ${PR_MYSQL_ROOT_PASS}
host = localhost
EOF
    sed -i .bak -e "s/{PR_MYSQL_USER_NAME}/$PR_MYSQL_USER_NAME/g" /usr/local/pronex/.env
    sed -i .bak -e "s/{PR_MYSQL_USER_PASS}/$PR_MYSQL_USER_PASS/g" /usr/local/pronex/.env
    sed -i .bak -e "s/{PR_MYSQL_DBNAME}/$PR_MYSQL_DBNAME/g" /usr/local/pronex/.env

    sed -i .bak -e "s/{PR_MYSQL_ROOT_PASS}/$PR_MYSQL_ROOT_PASS/g" /usr/local/pronex/install/PRHotspot.sql
    sed -i .bak -e "s/{PR_MYSQL_USER_NAME}/$PR_MYSQL_USER_NAME/g" /usr/local/pronex/install/PRHotspot.sql
    sed -i .bak -e "s/{PR_MYSQL_USER_PASS}/$PR_MYSQL_USER_PASS/g" /usr/local/pronex/install/PRHotspot.sql
    sed -i .bak -e "s/{PR_MYSQL_DBNAME}/$PR_MYSQL_DBNAME/g" /usr/local/pronex/install/PRHotspot.sql

    mysql --defaults-extra-file=/usr/local/pronex/install/client.cnf < /usr/local/pronex/install/PRHotspot.sql
    echo " [Tamam]" 1>&3

    # MySQL icin watchdog scripti olusturuluyor.
    echo -n "PRHotspot : MySQL icin watchdog scripti olusturuluyor." 1>&3
    cat <<EOF > /usr/local/bin/mysql_check.sh
#!/usr/bin/env sh
service mysql-server.sh status
if [ \$? != 0 ]; then
service mysql-server.sh start
fi
EOF
    chmod +x /usr/local/bin/mysql_check.sh
    echo " [Tamam]" 1>&3
}

_nginxSettings() {
    echo -n "PRHotspot : Web sunucusu kuruluyor..." 1>&3
    cp /usr/local/pronex/install/prhotspot.sh /usr/local/etc/rc.d/prhotspot.sh
    chmod +x /usr/local/etc/rc.d/prhotspot.sh
    if [ ! -f /etc/rc.conf.local ] || [ $(grep -c PRHotspot_enable /etc/rc.conf.local) -eq 0 ]; then
        echo "prhotspot_enable=\"YES\"" >> /etc/rc.conf.local
    fi
    sed -i .bak -e "s/{PR_PORT}/$PR_PORT/g" /usr/local/pronex/install/nginx-PRHotspot.conf
    echo " [Tamam]" 1>&3
}

_radiusInstall() {
    /usr/local/sbin/pfSsh.php playback listpkg | grep "freeradius3"
    if [ $? == 0 ]
    then
    echo -n "PRHotspot : FreeRADIUS3 zaten kurulu..." 1>&3
    else
    echo -n "PRHotspot : FreeRADIUS3 sistemde bulunamadi. Kuruluyor..." 1>&3
    /usr/local/sbin/pfSsh.php playback installpkg "freeradius2"
    hash -r
    fi
    echo " [Tamam]" 1>&3
}


_cronInstall() {
    /usr/local/sbin/pfSsh.php playback listpkg | grep "cron"
    if [ $? == 0 ]
    then
    echo -n "PRotspot : Cron zaten kurulu..." 1>&3
    else
    echo -n "PRotspot : Cron sistemde bulunamadi. Kuruluyor..." 1>&3
    /usr/local/sbin/pfSsh.php playback installpkg "cron"
    hash -r
    fi
    echo " [Tamam]" 1>&3
}

_PRHotspotSettings() {
    echo -n "PRHotspot : PRHotspot Konfigurasyon yukleniyor..." 1>&3
    cp /usr/local/pronex/install/PRHotspotconfig.php /etc/phpshellsessions/prhotspotconfig
    sed -i .bak -e "s/{PR_MYSQL_USER_NAME}/$PR_MYSQL_USER_NAME/g" /etc/phpshellsessions/prhotspotconfig
    sed -i .bak -e "s/{PR_MYSQL_USER_PASS}/$PR_MYSQL_USER_PASS/g" /etc/phpshellsessions/prhotspotconfig
    sed -i .bak -e "s/{PR_MYSQL_DBNAME}/$PR_MYSQL_DBNAME/g" /etc/phpshellsessions/prhotspotconfig
    /usr/local/sbin/pfSsh.php playback prhotspotconfig
    echo " [Tamam]" 1>&3
}

_prpanel() {
    echo -n "PRHotspot : Panel Yukleniyor...(Islem uzun surebilir)" 1>&3
    cd /usr/local/pronex
    echo "suhosin.executor.include.whitelist = phar" >> /usr/local/etc/php.ini
    sleep 5
    php -d memory_limit=-1 /usr/local/bin/composer.phar install --no-dev
    php artisan key:generate
    php artisan migrate --seed --force
    echo " [Tamam]" 1>&3
}



_proxy() {
     /usr/local/sbin/pfSsh.php playback listpkg | grep "squid"
    if [ $? == 0 ]
    then
    echo -n "PRotspot : Squid paketleri sistemde zaten kurulu..." 1>&3
    else
    echo -n "PRotspot : Squid paketleri sistemde bulunamadi. Kuruluyor..." 1>&3
    /usr/local/sbin/pfSsh.php playback installpkg "squid"
    /usr/local/sbin/pfSsh.php playback installpkg "lightsquid"
        /usr/local/sbin/pfSsh.php playback installpkg "squidguard"


    hash -r
    fi
    echo " [Tamam]" 1>&3
}


_5651() {
  cd /usr/local/pronex/install
  sh 5651.sh
}

YesOrNo() {
    while :
    do
        echo -n "$1 (yes/no?): " 1>&3
        read -p "$1 (yes/no?): " answer
        case "${answer}" in
            [yY]|[yY][eE][sS]) exit 0 ;;
                [nN]|[nN][oO]) exit 1 ;;
        esac
    done
}

main
