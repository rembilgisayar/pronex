#!/bin/sh
SSL_CN="log.pronex.com.tr"
SSL_EMAIL="info@rembilgisayar.com.tr"
SSL_O="Pronex"
SSL_C="TR"
SSL_ST="Istanbul"
SSL_L="Mecidiyekoy"
# Gerekirse sifirla
rm -rf /root/.openssl/password.txt /root/.openssl/CA/ /root/.openssl/ssl/ /usr/local/www/log_browser /sbin/logimza-imzala.sh /sbin/dhcptibduzenle.sh
# Zaman damgasi icin OpenSSL ayarlari
mkdir -p /root/.openssl

fetch https://bitbucket.org/mono/pfsense-5651/raw/master/openssl.cnf -o /root/.openssl/openssl.cnf

#fetch http://www.semihbilisim.com/scripts/openssl.cnf -o /root/.openssl/openssl.cnf
# Sertifika icin rasgele sifre olusturuyoruz
touch /root/.openssl/password.txt
openssl rand -base64 32 > /root/.openssl/password.txt
cat /root/.openssl/password.txt
# Sertifika olusturma islemleri
# Gerekli klasor ve dosyalari olustur
mkdir -p /root/.openssl/ssl
mkdir -p /root/.openssl/CA/private
mkdir -p /root/.openssl/CA/newcerts
touch /root/.openssl/CA/index.txt
touch /root/.openssl/CA/serial
echo 011E > /root/.openssl/CA/serial
touch /root/.openssl/CA/tsaserial
echo 011E > /root/.openssl/CA/tsaserial
# CA olustur
cd /root/.openssl/ssl
openssl req -config /root/.openssl/openssl.cnf -passout file:/root/.openssl/password.txt -days 3650 -x509 -newkey rsa:2048 -sha256 -subj "/CN=$SSL_CN/emailAddress=$SSL_EMAIL/O=$SSL_O/C=$SSL_C/ST=$SSL_ST/L=$SSL_L" -out /root/.openssl/ssl/cacert.pem -outform PEM
cp /root/.openssl/ssl/cacert.pem /root/.openssl/CA/
cp /root/.openssl/ssl/privkey.pem /root/.openssl/CA/private/cakey.pem
# TSA icin Sertifika olustur
openssl genrsa -aes256 -passout file:/root/.openssl/password.txt -out /root/.openssl/ssl/tsakey.pem 2048
openssl req -new -key /root/.openssl/ssl/tsakey.pem -passin file:/root/.openssl/password.txt -sha256 -out /root/.openssl/ssl/tsareq.csr
openssl ca -config /root/.openssl/openssl.cnf -passin file:/root/.openssl/password.txt -days 3650 -batch -in /root/.openssl/ssl/tsareq.csr -subj "/CN=$SSL_CN/emailAddress=$SSL_EMAIL/O=$SSL_O/C=$SSL_C/ST=$SSL_ST/L=$SSL_L" -out /root/.openssl/ssl/tsacert.pem
cp /root/.openssl/ssl/tsacert.pem /root/.openssl/CA/
cp /root/.openssl/ssl/tsakey.pem /root/.openssl/CA/private/
# log_browser ve imzalama betiklerini yukle
fetch https://github.com/monobilisim/log_browser/archive/master.zip -o /tmp/log_browser.zip
#fetch http://www.semihbilisim.com/log_browser-master.zip -o /tmp/log_browser.zip
unzip -d /usr/local/www /tmp/log_browser.zip
mv /usr/local/www/log_browser-master /usr/local/www/log_browser
rm /tmp/log_browser.zip
fetch https://bitbucket.org/mono/pfsense-5651/raw/master/logimza-imzala.sh -o /sbin/logimza-imzala.sh
fetch https://bitbucket.org/mono/pfsense-5651/raw/master/dhcptibduzenle.sh -o /sbin/dhcptibduzenle.sh
#fetch http://www.semihbilisim.com/scripts/logimza-imzala.sh -o /sbin/logimza-imzala.sh
#fetch http://www.semihbilisim.com/scripts/dhcptibduzenle.sh -o /sbin/dhcptibduzenle.sh
chmod +x /sbin/logimza-imzala.sh /sbin/dhcptibduzenle.sh
mkdir /logimza