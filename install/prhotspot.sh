#!/usr/bin/env sh

. /etc/rc.subr

name="pronex"
rcvar="pronex_enable"
start_cmd="pronex_start"
stop_cmd="pronex_stop"

pidfile="/var/run/${name}.pid"
config_file="/usr/local/pronex/install/nginx-PRHotspot.conf"

load_rc_config ${name}

pronex_start()
{
if checkyesno ${rcvar}; then
    if [ -f $pidfile ]; then
        echo "pronex zaten çalisiyor."
        qhotspot_stop
    fi
    echo -n "pronex calistiriliyor."
    /usr/local/sbin/nginx -c ${config_file}
    echo " calistirildi."
fi
}

pronex_stop()
{
if [ -f $pidfile ]; then
        echo -n "pronex durduruluyor."
        pkill -F $pidfile
        while [ `pgrep -F $pidfile` ]; do
            echo -n "."
            sleep 5
        done
        rm $pidfile
        echo " durduruldu."
    fi
}

run_rc_command "$1"